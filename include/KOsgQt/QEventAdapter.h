/* -*-c++-*- OpenSceneGraph - Copyright (C) 2009 Wang Rui, 2016 Kalkulo AS
 *
 * This library is open source and may be redistributed and/or modified under
 * the terms of the OpenSceneGraph Public License (OSGPL) version 0.0 or
 * (at your option) any later version.  The full license is in LICENSE file
 * included with this distribution, and on the openscenegraph.org website.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * OpenSceneGraph Public License for more details.
*/

#pragma once

#include <QSize>
#include <osgGA/EventQueue>
#include <memory>

#include <KOsgQt/Export.h>

namespace KOsgQt
{

class ViewerWidgetQt;

/** Intercepts and forwards events from a QWidget */
class KOSGQT_EXPORT QEventAdapter
{
public:

    QEventAdapter(ViewerWidgetQt* widget, osgGA::EventQueue*);
    ~QEventAdapter();

    void setSize(int width, int height, bool fixed);
    QSize size() const;
    bool fixedSize() const;

private:

    class Private;
    std::unique_ptr<Private> p_;

};

}
