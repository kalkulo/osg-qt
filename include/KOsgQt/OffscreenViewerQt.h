/* -*-c++-*- OpenSceneGraph - Copyright (C) 2016 Kalkulo AS
 *
 * This library is open source and may be redistributed and/or modified under
 * the terms of the OpenSceneGraph Public License (OSGPL) version 0.0 or
 * (at your option) any later version.  The full license is in LICENSE file
 * included with this distribution, and on the openscenegraph.org website.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * OpenSceneGraph Public License for more details.
*/
#pragma once

#include <osgViewer/Viewer>
#include <QObject>
#include <QTimer>
#include <QImage>

#include <memory>

#include <KOsgQt/Export.h>

namespace KOsgQt {

class PixelBufferQt; // fwd

/**
 * Off-screen rendering with Qt5. NOTE: Should always be heap allocated and
 * stored in a ref_ptr, for safe interaction between callbacks and signals.
 */
class KOSGQT_EXPORT OffscreenViewerQt : public QObject, public osgViewer::Viewer
{
    Q_OBJECT

public:

    /** See, e.g., http://lists.apple.com/archives/mac-opengl/2010/May/msg00027.html */
    static const GLenum glPixelFormat = GL_BGRA;
    static const GLenum glPixelType = GL_UNSIGNED_INT_8_8_8_8_REV;

    OffscreenViewerQt(PixelBufferQt*);
    virtual ~OffscreenViewerQt();

    /** Ensure no accidental copying. */
    OffscreenViewerQt(const OffscreenViewerQt&) = delete;
    OffscreenViewerQt& operator=(const OffscreenViewerQt&) = delete;

    /** Override superclass methods to work around various limitations. */
    void realize() override;
    void requestRedraw() override;
    void startThreading() override;
    void setStartTick(osg::Timer_t) override;

public slots:

    /** Implement Qt based async frame loop. */
    void start();

    /** Stop frame loop. */
    void stop();

public:

    /** Check if the frame loop is active. */
    bool running() const;

signals:

    /** Signal emitted before a new frame is rendered */
    void newFrame();

    /** Signal after a new frame was rendered */
    void endFrame();

protected:

    /** Called by Qt timer to trigger a new frame. */
    void timerEvent(QTimerEvent*) override;

private:

    class Private;
    std::unique_ptr<Private> p_;

};

} // namespace
