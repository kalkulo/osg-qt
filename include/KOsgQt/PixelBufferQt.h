/* -*-c++-*- OpenSceneGraph - Copyright (C) 2009 Wang Rui, 2016 Kalkulo AS
 *
 * This library is open source and may be redistributed and/or modified under
 * the terms of the OpenSceneGraph Public License (OSGPL) version 0.0 or
 * (at your option) any later version.  The full license is in LICENSE file
 * included with this distribution, and on the openscenegraph.org website.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * OpenSceneGraph Public License for more details.
*/
#pragma once

#include <QObject>
#include <osg/GraphicsContext>
#include <KOsgQt/Export.h>
#include <memory>

class QOffscreenSurface;
class QOpenGLContext;
class QOpenGLFramebufferObject;

namespace KOsgQt
{

class KOSGQT_EXPORT PixelBufferQt : public QObject, public osg::GraphicsContext
{
    Q_OBJECT;

public:

    PixelBufferQt(osg::GraphicsContext::Traits* traits=nullptr);
    ~PixelBufferQt();

    void setShareContext(QOpenGLContext*);
    QOpenGLContext* getContext(); // unused?

    /* Should be override, but parent method not virtual. See OffscreenViewerQt::startThreading() */
    void createGraphicsThread();

    /* Implementation */

    bool valid() const override;
    bool realizeImplementation() override;
    bool isRealizedImplementation() const override;
    void closeImplementation() override;
    bool makeCurrentImplementation() override;
    bool makeContextCurrentImplementation(osg::GraphicsContext*) override;
    bool releaseContextImplementation() override;
    void bindPBufferToTextureImplementation(GLenum buffer) override;
    void swapBuffersImplementation() override;
    void resizedImplementation(int x, int y, int width, int height) override;
    void runOperations() override;

    class FBOPool;
    class KOSGQT_EXPORT FBOFromPool
    {
    public:

        FBOFromPool();
        FBOFromPool(FBOPool* owner, QOpenGLFramebufferObject* fbo);

        bool valid() const;
        int width() const;
        int height() const;
        GLuint texture() const;

    private:
        class Shared;
        std::shared_ptr<Shared> s_;
    };

signals:

    void newFrame(const FBOFromPool&);
    void bringup();

private:

    class Private;
    std::unique_ptr<Private> p_;

};

}
