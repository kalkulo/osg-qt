/* -*-c++-*- OpenSceneGraph - Copyright (C) 2016 Kalkulo AS
 *
 * This library is open source and may be redistributed and/or modified under
 * the terms of the OpenSceneGraph Public License (OSGPL) version 0.0 or
 * (at your option) any later version.  The full license is in LICENSE file
 * included with this distribution, and on the openscenegraph.org website.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * OpenSceneGraph Public License for more details.
*/
#pragma once

#include <QOpenGLWidget>
#include <KOsgQt/PixelBufferQt.h>

#include <memory>

#include <KOsgQt/Export.h>

namespace osg { class View; }
namespace osgGA { class EventQueue; }
namespace osgViewer { class Viewer; }

namespace KOsgQt {

class OffscreenViewerQt; // fwd

/** Widget for displaying and interacting with off-screen rendered OSG
 * content. Note: In order for the widget to be reparentable,
 * Qt::AA_ShareOpenGLContexts must be set on the application. */
class KOSGQT_EXPORT ViewerWidgetQt : public QOpenGLWidget
{
    Q_OBJECT;

public:

    ViewerWidgetQt(QWidget* parent=nullptr, osg::GraphicsContext::Traits* traits=nullptr);
    ~ViewerWidgetQt();

    void paintGL() override;
    void initializeGL() override;
    void resizeGL(int w, int h) override;

    void setViewportSize(int width, int height);
    QSize getViewportSize() const;

    QImage grabFrameBuffer(bool include_alpha=false);

    /** Get viewer/view for widget. All these return the same object, but differ in intended usage */
    OffscreenViewerQt* getViewerQt();
    osgViewer::Viewer* getViewer();
    osg::View* getView();

signals:
    void frameBufferObjectUpdated();

protected slots:

    /** Called after the offscreen-context is made current the first time. The thread may be different from the GUI
     * thread. */
    virtual void initializeOffscreenGL();

    void newFrame(const PixelBufferQt::FBOFromPool&);
    void closeGL();

protected:

    PixelBufferQt* getPixelBufferQt();
    osgGA::EventQueue* getEventQueue();

private:

    class Private;
    std::unique_ptr<Private> p_;

};

} // namespace
