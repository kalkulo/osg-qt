/* -*-c++-*- OpenSceneGraph - Copyright (C) 2016 Kalkulo AS
 *
 * This library is open source and may be redistributed and/or modified under
 * the terms of the OpenSceneGraph Public License (OSGPL) version 0.0 or
 * (at your option) any later version.  The full license is in LICENSE file
 * included with this distribution, and on the openscenegraph.org website.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * OpenSceneGraph Public License for more details.
*/

#include <KOsgQt/OffscreenViewerQt.h>
#include <KOsgQt/PixelBufferQt.h>

#include <QOpenGLContext>
#include <QThread>

#include <atomic>
#include <iostream>

using namespace KOsgQt;

namespace {
    class ResizedHandler : public osgGA::GUIEventHandler
    {
    public:
        bool handle(const osgGA::GUIEventAdapter&, osgGA::GUIActionAdapter&, osg::Object*, osg::NodeVisitor*) override;
    };
}

//----------------------------------------------------------
bool ResizedHandler::handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa, osg::Object*, osg::NodeVisitor*)
//----------------------------------------------------------
{
    if (ea.getEventType() == osgGA::GUIEventAdapter::RESIZE) {
        const int w = ea.getWindowWidth();
        const int h = ea.getWindowHeight();
        aa.asView()->getCamera()->getGraphicsContext()->resized(0, 0, w, h);
    }
    return false;
}

class OffscreenViewerQt::Private
{
public:
    int timer_id_ = 0;
    std::atomic<bool> redraw_requested_ = {false};
};

//----------------------------------------------------------
OffscreenViewerQt::OffscreenViewerQt(PixelBufferQt* gc)
//----------------------------------------------------------
    : p_(new Private())
{
    const osg::GraphicsContext::Traits* traits = gc->getTraits();
    getCamera()->setGraphicsContext(gc);
    getCamera()->setViewport(0, 0, traits->width, traits->height);
    addEventHandler(new ResizedHandler);
    // By default Osg will set the cpu affinity and pin each thread to
    // particular CPUs. In particular it looks like it will pin the
    // calling thread to CPU 0, which will then be inherited if the
    // calling thread spawns new threads at a later point. To avoid
    // this we turn off the affinity settings. - Ulf
    osgViewer::Viewer::setUseConfigureAffinity(false);
}

//----------------------------------------------------------
OffscreenViewerQt::~OffscreenViewerQt()
//----------------------------------------------------------
{
    // If there is a graphics thread, cancel it so that the QOpenGLContext is
    // moved back to the main thread.
    osg::GraphicsContext* gc = getCamera()->getGraphicsContext();
    osg::ref_ptr<osg::GraphicsThread> thread = gc->getGraphicsThread();
    if (thread) {
        gc->setGraphicsThread(nullptr);
        thread->join();
    }
}

//----------------------------------------------------------
void OffscreenViewerQt::realize()
//----------------------------------------------------------
{
    if (dynamic_cast<PixelBufferQt*>(getCamera()->getGraphicsContext()) == nullptr)
        OSG_WARN << "OffscreenViewerQt should be paired with a PixelBufferQt graphics context";
    osgViewer::Viewer::realize();

}

//----------------------------------------------------------
void OffscreenViewerQt::requestRedraw()
//----------------------------------------------------------
{
    // osgViewer::ViewerBase::_requestRedraw is set to false _after_ the frame
    // is completed, hence calling requestRedraw from an event handler will not
    // work. So we need to handle it ourself.
    p_->redraw_requested_ = true;
}

//----------------------------------------------------------
void OffscreenViewerQt::startThreading()
//----------------------------------------------------------
{
    // osg::GraphicsContext::createGraphicsThread() is not virtual, but we need
    // to control the thread that is created (to modify the thread-pinning of
    // the context). Hence, we create the thread here, so that it already exists
    // when super::startThreading runs.
    PixelBufferQt* gc = dynamic_cast<PixelBufferQt*>(getCamera()->getGraphicsContext());
    gc->createGraphicsThread();
    osgViewer::Viewer::startThreading();
}

//----------------------------------------------------------
void OffscreenViewerQt::setStartTick(osg::Timer_t timer)
//----------------------------------------------------------
{
    osgViewer::Viewer::setStartTick(timer);
    // setStartTick, called by setSceneData, clears the event queue - draining
    // events holding the initial window size. Synthezise an event to
    // compensate.
    osgGA::EventQueue* eq = osgViewer::View::getEventQueue();
    osgGA::GUIEventAdapter* ea = eq->getCurrentEventState();
    eq->windowResize(0, 0, ea->getWindowWidth(), ea->getWindowHeight());
}

//----------------------------------------------------------
void OffscreenViewerQt::start()
//----------------------------------------------------------
{
    if (thread() != QThread::currentThread()) {
        // Call again in correct thread
        QMetaObject::invokeMethod(this, "start", Qt::QueuedConnection);
        return;
    }

    p_->timer_id_ = startTimer(1000/64);
}

//----------------------------------------------------------
void OffscreenViewerQt::stop()
//----------------------------------------------------------
{
    if (!running())
        return;

    if (thread() != QThread::currentThread()) {
        // Call again in correct thread
        QMetaObject::invokeMethod(this, "stop", Qt::QueuedConnection);
        return;
    }

    killTimer(p_->timer_id_);
    p_->timer_id_ = 0;
}

//----------------------------------------------------------
bool OffscreenViewerQt::running() const
//----------------------------------------------------------
{
    return (p_->timer_id_ != 0);
}

//----------------------------------------------------------
void OffscreenViewerQt::timerEvent(QTimerEvent*)
//----------------------------------------------------------
{
    bool doFrame = p_->redraw_requested_.exchange(false)
        || (getRunFrameScheme() == osgViewer::Viewer::CONTINUOUS);

    if (!doFrame) {
        // Traverse events early, in case it activates checkNeedToDoFrame
        eventTraversal();
        doFrame = checkNeedToDoFrame();
    }

    if (doFrame) {
        emit newFrame();
        frame();
        emit endFrame();
    }
}
