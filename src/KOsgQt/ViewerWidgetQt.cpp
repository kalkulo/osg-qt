/* -*-c++-*- OpenSceneGraph - Copyright (C) 2016 Kalkulo AS
 *
 * This library is open source and may be redistributed and/or modified under
 * the terms of the OpenSceneGraph Public License (OSGPL) version 0.0 or
 * (at your option) any later version.  The full license is in LICENSE file
 * included with this distribution, and on the openscenegraph.org website.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * OpenSceneGraph Public License for more details.
*/


/*
 * This class should inherit from QOpenGLWidget. Override paintGL() to blit
 * the PixelBufferQt texture to the widget's fbo.
 *
 * Should contain:
 * ViewerQt (not created yet) - frame runner
 *   PixelBufferQt (GraphicsContext)
 *   osgGA::EventQueue, connected to PixelBufferQt
 * QEventAdapter (widget -> eventqueue)
 */


#include <KOsgQt/ViewerWidgetQt.h>

#include <QCoreApplication>
#include <QOpenGLContext>
#include <QOpenGLFunctions_2_0>
#include <QThread>

#include <osgGA/GUIEventAdapter>
#include <osgGA/EventQueue>

#include <KOsgQt/OffscreenViewerQt.h>
#include <KOsgQt/QEventAdapter.h>

// Testing
#include <osgDB/ReadFile>

using namespace KOsgQt;

class ViewerWidgetQt::Private
{
public:
    Private(ViewerWidgetQt*, osg::GraphicsContext::Traits*);
    osg::ref_ptr<PixelBufferQt> pixelbuffer_;
    osg::ref_ptr<OffscreenViewerQt> renderer_;
    osg::ref_ptr<osgGA::EventQueue> eventq_;
    QEventAdapter adapter_;
    PixelBufferQt::FBOFromPool fbo_;
    std::unique_ptr<QOpenGLFunctions_2_0> glfunc_; // old GL version, simplifies paintGL
};

//----------------------------------------------------------
ViewerWidgetQt::Private::Private(ViewerWidgetQt* parent, osg::GraphicsContext::Traits* traits)
//----------------------------------------------------------
    : pixelbuffer_(new PixelBufferQt(traits))
    , renderer_(new OffscreenViewerQt(pixelbuffer_))
    , eventq_(new osgGA::EventQueue(osgGA::GUIEventAdapter::Y_INCREASING_UPWARDS))
    , adapter_(parent, eventq_)
{
    renderer_->setEventQueue(eventq_);


    QObject::connect(pixelbuffer_, &PixelBufferQt::bringup, parent, &ViewerWidgetQt::initializeOffscreenGL, Qt::DirectConnection);
}

//----------------------------------------------------------
ViewerWidgetQt::ViewerWidgetQt(QWidget* parent,  osg::GraphicsContext::Traits* traits)
//----------------------------------------------------------
    : QOpenGLWidget(parent)
    , p_(new Private(this, traits))
{
    setFocusPolicy(Qt::WheelFocus);
    setMouseTracking(true);
}

//----------------------------------------------------------
ViewerWidgetQt::~ViewerWidgetQt()
//----------------------------------------------------------
{
    if (p_->renderer_.valid())
        p_->renderer_->stop();
    if (context())
      disconnect(context(), &QOpenGLContext::aboutToBeDestroyed, this, &ViewerWidgetQt::closeGL);
}

//----------------------------------------------------------
void ViewerWidgetQt::initializeOffscreenGL()
//----------------------------------------------------------
{
    // noting
}

//----------------------------------------------------------
void ViewerWidgetQt::initializeGL()
//----------------------------------------------------------
{
    QOpenGLWidget::initializeGL();
    p_->glfunc_.reset(new QOpenGLFunctions_2_0());
    p_->glfunc_->initializeOpenGLFunctions();

    p_->pixelbuffer_->setShareContext(context());
    connect(p_->pixelbuffer_, &PixelBufferQt::newFrame, this, &ViewerWidgetQt::newFrame);
    connect(context(), &QOpenGLContext::aboutToBeDestroyed, this, &ViewerWidgetQt::closeGL);

    p_->glfunc_->glDisable(GL_LIGHTING);

    // The clear color is only ever visible outside the texture, i.e. on resize
    // before updated frame is received. Make it a recognizable color.
    p_->glfunc_->glClearColor(0.6, 0.6, 1.0, 1.0);

    // With the default blending, the desktop background shines through where
    // the texture is transparent. To avoid that, we set up a blend function to
    // removes the transparency (by adding opaque black to the source color).
    if (!testAttribute(Qt::WA_TranslucentBackground)) {
        p_->glfunc_->glEnable(GL_BLEND);
        p_->glfunc_->glBlendColor(0.0, 0.0, 0.0, 1.0);
        p_->glfunc_->glBlendFunc(GL_ONE, GL_CONSTANT_COLOR);

        // It can be really slow to read back the previous content of the
        // window. Avoid that by specifying that we always overpaint everything
        // so that read-back is not performed.
        setAttribute(Qt::WA_OpaquePaintEvent, true);
    }

    p_->renderer_->realize();
    p_->renderer_->start();
}

//----------------------------------------------------------
void ViewerWidgetQt::closeGL()
//----------------------------------------------------------
{
    if (!QCoreApplication::testAttribute(Qt::AA_ShareOpenGLContexts)) {
        OSG_WARN << "Switching OpenGL context not supported. Set Qt::AA_ShareOpenGLContexts to avoid it." << std::endl;
    }
    // This avoids a crash due to glfunc using context in destructor (qt bug in early 5.x versions?). Can probably be
    // removed now, but testing required.
    p_->glfunc_.reset();
}

//----------------------------------------------------------
void ViewerWidgetQt::paintGL()
//----------------------------------------------------------
{
    //p_->glfunc_->glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT|GL_STENCIL_BUFFER_BIT);
    if (!p_->fbo_.valid())
        return;

    double relwidth, relheight;
    if (p_->adapter_.fixedSize()) {
        /* Center the texture in the window, preserving resolution */
        const int dpr = devicePixelRatio();
        relwidth = (double) p_->fbo_.width() / (dpr * width());
        relheight = (double) p_->fbo_.height() / (dpr * height());
    } else {
        /* The texture size may lag during a resize. We want to fill the window
         * vertically, while preserving aspect ratio. */
        relwidth = 1.0 * (height() * p_->fbo_.width()) / (width() * p_->fbo_.height());
        relheight = 1.0;
    }
    p_->glfunc_->glEnable(GL_TEXTURE_2D); {
        // Draw a centered quad, textured by the last rendered frame. Current
        // projection is unit-cube ortographic.
        p_->glfunc_->glBindTexture(GL_TEXTURE_2D, p_->fbo_.texture());
        p_->glfunc_->glBegin(GL_QUADS); {
            p_->glfunc_->glTexCoord2f(0, 0); p_->glfunc_->glVertex3f(-relwidth, -relheight, 0);
            p_->glfunc_->glTexCoord2f(1, 0); p_->glfunc_->glVertex3f( relwidth, -relheight, 0);
            p_->glfunc_->glTexCoord2f(1, 1); p_->glfunc_->glVertex3f( relwidth,  relheight, 0);
            p_->glfunc_->glTexCoord2f(0, 1); p_->glfunc_->glVertex3f(-relwidth,  relheight, 0);
        } p_->glfunc_->glEnd();

    } p_->glfunc_->glDisable(GL_TEXTURE_2D);
}

//----------------------------------------------------------
void ViewerWidgetQt::resizeGL(int w, int h)
//----------------------------------------------------------
{
    QOpenGLWidget::resizeGL(w, h);
}

//----------------------------------------------------------
void ViewerWidgetQt::newFrame(const PixelBufferQt::FBOFromPool& fbo)
//----------------------------------------------------------
{
    p_->fbo_ = fbo;
    update();

    emit frameBufferObjectUpdated();
}

//----------------------------------------------------------
OffscreenViewerQt* ViewerWidgetQt::getViewerQt()
//----------------------------------------------------------
{
    return p_->renderer_;
}

//----------------------------------------------------------
osgViewer::Viewer* ViewerWidgetQt::getViewer()
//----------------------------------------------------------
{
    return p_->renderer_;
}

//----------------------------------------------------------
osg::View* ViewerWidgetQt::getView()
//----------------------------------------------------------
{
    return p_->renderer_;
}

//----------------------------------------------------------
osgGA::EventQueue* ViewerWidgetQt::getEventQueue()
//----------------------------------------------------------
{
    return p_->eventq_;
}

//----------------------------------------------------------
PixelBufferQt* ViewerWidgetQt::getPixelBufferQt()
//----------------------------------------------------------
{
    return p_->pixelbuffer_;
}

//----------------------------------------------------------
void ViewerWidgetQt::setViewportSize(int width, int height)
//----------------------------------------------------------
{
   int dpr = devicePixelRatio();

   if(width > 0 && height > 0)
      p_->adapter_.setSize(width, height, true);
   else
      p_->adapter_.setSize(dpr*this->width(), dpr*this->height(), false);

   emit resized();
}

//----------------------------------------------------------
QSize ViewerWidgetQt::getViewportSize() const
//----------------------------------------------------------
{
    return p_->adapter_.size();
}

//----------------------------------------------------------
QImage ViewerWidgetQt::grabFrameBuffer(bool include_alpha)
//----------------------------------------------------------
{
    if (!p_->fbo_.valid())
        return QImage();

    QImage img(p_->fbo_.width(), p_->fbo_.height(), include_alpha ? QImage::Format_ARGB32 : QImage::Format_RGB32);

    makeCurrent();
    p_->glfunc_->glEnable(GL_TEXTURE_2D); {
        p_->glfunc_->glBindTexture(GL_TEXTURE_2D, p_->fbo_.texture());
        p_->glfunc_->glGetTexImage(GL_TEXTURE_2D, 0, GL_BGRA, GL_UNSIGNED_INT_8_8_8_8_REV, img.bits());
    } p_->glfunc_->glDisable(GL_TEXTURE_2D);
    doneCurrent();

    return img.mirrored(false, true);
}
