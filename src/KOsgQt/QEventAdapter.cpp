/* -*-c++-*- OpenSceneGraph - Copyright (C) 2009 Wang Rui, 2016 Kalkulo AS
 *
 * This library is open source and may be redistributed and/or modified under
 * the terms of the OpenSceneGraph Public License (OSGPL) version 0.0 or
 * (at your option) any later version.  The full license is in LICENSE file
 * included with this distribution, and on the openscenegraph.org website.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * OpenSceneGraph Public License for more details.
*/
#include <KOsgQt/QEventAdapter.h>
#include <KOsgQt/ViewerWidgetQt.h>

#include <osg/View>

#include <QEvent>
#include <QGestureEvent>
#include <QInputEvent>
#include <QKeyEvent>
#include <QMoveEvent>
#include <QMouseEvent>
#include <QResizeEvent>
#include <QWheelEvent>
#include <QWidget>

#include <QGuiApplication>
#include <iostream>

#if (QT_VERSION>=QT_VERSION_CHECK(4, 6, 0))
# define USE_GESTURES
# include <QGestureEvent>
# include <QGesture>
#endif

using namespace KOsgQt;

class QEventAdapter::Private : public QObject
{
public:

    Private(ViewerWidgetQt*, osgGA::EventQueue*);

    bool eventFilter(QObject*, QEvent*) override;
    void setSize(int width, int height, bool fixed);
    QSize size() const;
    bool fixedSize() const;

private:

    /* Functions to handle different types of events. Mirrors those in QWidget. */
    void keyPressEvent(QKeyEvent*);
    void keyReleaseEvent(QKeyEvent*);
    void mousePressEvent(QMouseEvent*);
    void mouseReleaseEvent(QMouseEvent*);
    void mouseDoubleClickEvent(QMouseEvent*);
    void mouseMoveEvent(QMouseEvent*);
    void wheelEvent(QWheelEvent*);
    void gestureEvent(QGestureEvent*);
    void showEvent(QShowEvent*);

    void resizeEvent(QResizeEvent*);
    void moveEvent(QMoveEvent*);

    /* Common function to set keyboard modifiers. */
    void setKeyboardModifierState(QInputEvent*);

    /* Utility functions */
    int remapKey(QKeyEvent*) const;
    int remapBtn(QMouseEvent*) const;
    osgGA::GUIEventAdapter::ScrollingMotion remapWheel(QWheelEvent*) const;
    osgGA::GUIEventAdapter::TouchPhase remapGesture(Qt::GestureState) const;
    unsigned int remapModifiers(Qt::KeyboardModifiers) const;
    std::pair<int, int> remapXY(QMouseEvent*) const;

    std::map<unsigned int, int> keymap_;
    ViewerWidgetQt* widget_;
    osg::ref_ptr<osgGA::EventQueue> eventq_;

    int width_;
    int height_;
    bool fixedSize_;
};

//----------------------------------------------------------
QEventAdapter::Private::Private(ViewerWidgetQt* widget, osgGA::EventQueue* eventq)
//----------------------------------------------------------
    : widget_(widget), eventq_(eventq), width_(1), height_(1), fixedSize_(false)
{
    //widget_->grabGesture(Qt::PinchGesture);

    //keymap_[Qt::Key_Escape    ] = osgGA::GUIEventAdapter::KEY_Escape;
    keymap_[Qt::Key_Escape    ] = 0x0000;
    keymap_[Qt::Key_Delete    ] = osgGA::GUIEventAdapter::KEY_Delete;
    keymap_[Qt::Key_Home      ] = osgGA::GUIEventAdapter::KEY_Home;
    keymap_[Qt::Key_Enter     ] = osgGA::GUIEventAdapter::KEY_KP_Enter;
    keymap_[Qt::Key_End       ] = osgGA::GUIEventAdapter::KEY_End;
    keymap_[Qt::Key_Return    ] = osgGA::GUIEventAdapter::KEY_Return;
    keymap_[Qt::Key_PageUp    ] = osgGA::GUIEventAdapter::KEY_Page_Up;
    keymap_[Qt::Key_PageDown  ] = osgGA::GUIEventAdapter::KEY_Page_Down;
    keymap_[Qt::Key_Left      ] = osgGA::GUIEventAdapter::KEY_Left;
    keymap_[Qt::Key_Right     ] = osgGA::GUIEventAdapter::KEY_Right;
    keymap_[Qt::Key_Up        ] = osgGA::GUIEventAdapter::KEY_Up;
    keymap_[Qt::Key_Down      ] = osgGA::GUIEventAdapter::KEY_Down;
    keymap_[Qt::Key_Backspace ] = osgGA::GUIEventAdapter::KEY_BackSpace;
    keymap_[Qt::Key_Tab       ] = osgGA::GUIEventAdapter::KEY_Tab;
    keymap_[Qt::Key_Space     ] = osgGA::GUIEventAdapter::KEY_Space;
    keymap_[Qt::Key_Delete    ] = osgGA::GUIEventAdapter::KEY_Delete;
    keymap_[Qt::Key_Alt       ] = osgGA::GUIEventAdapter::KEY_Alt_L;
    keymap_[Qt::Key_Shift     ] = osgGA::GUIEventAdapter::KEY_Shift_L;
    keymap_[Qt::Key_Control   ] = osgGA::GUIEventAdapter::KEY_Control_L;
    keymap_[Qt::Key_Meta      ] = osgGA::GUIEventAdapter::KEY_Meta_L;

    keymap_[Qt::Key_F1        ] = osgGA::GUIEventAdapter::KEY_F1;
    keymap_[Qt::Key_F2        ] = osgGA::GUIEventAdapter::KEY_F2;
    keymap_[Qt::Key_F3        ] = osgGA::GUIEventAdapter::KEY_F3;
    keymap_[Qt::Key_F4        ] = osgGA::GUIEventAdapter::KEY_F4;
    keymap_[Qt::Key_F5        ] = osgGA::GUIEventAdapter::KEY_F5;
    keymap_[Qt::Key_F6        ] = osgGA::GUIEventAdapter::KEY_F6;
    keymap_[Qt::Key_F7        ] = osgGA::GUIEventAdapter::KEY_F7;
    keymap_[Qt::Key_F8        ] = osgGA::GUIEventAdapter::KEY_F8;
    keymap_[Qt::Key_F9        ] = osgGA::GUIEventAdapter::KEY_F9;
    keymap_[Qt::Key_F10       ] = osgGA::GUIEventAdapter::KEY_F10;
    keymap_[Qt::Key_F11       ] = osgGA::GUIEventAdapter::KEY_F11;
    keymap_[Qt::Key_F12       ] = osgGA::GUIEventAdapter::KEY_F12;
    keymap_[Qt::Key_F13       ] = osgGA::GUIEventAdapter::KEY_F13;
    keymap_[Qt::Key_F14       ] = osgGA::GUIEventAdapter::KEY_F14;
    keymap_[Qt::Key_F15       ] = osgGA::GUIEventAdapter::KEY_F15;
    keymap_[Qt::Key_F16       ] = osgGA::GUIEventAdapter::KEY_F16;
    keymap_[Qt::Key_F17       ] = osgGA::GUIEventAdapter::KEY_F17;
    keymap_[Qt::Key_F18       ] = osgGA::GUIEventAdapter::KEY_F18;
    keymap_[Qt::Key_F19       ] = osgGA::GUIEventAdapter::KEY_F19;
    keymap_[Qt::Key_F20       ] = osgGA::GUIEventAdapter::KEY_F20;

    keymap_[Qt::Key_hyphen    ] = '-';
    keymap_[Qt::Key_Equal     ] = '=';

    keymap_[Qt::Key_division  ] = osgGA::GUIEventAdapter::KEY_KP_Divide;
    keymap_[Qt::Key_multiply  ] = osgGA::GUIEventAdapter::KEY_KP_Multiply;
    keymap_[Qt::Key_Minus     ] = '-';
    keymap_[Qt::Key_Plus      ] = '+';
}

//----------------------------------------------------------
int QEventAdapter::Private::remapKey(QKeyEvent* event) const
//----------------------------------------------------------
{
    auto it = keymap_.find(event->key());
    return (it!=keymap_.end() ? it->second : (int)event->text()[0].toLatin1());
}

//----------------------------------------------------------
int QEventAdapter::Private::remapBtn(QMouseEvent* event) const
//----------------------------------------------------------
{
    switch (event->button()) {
    case Qt::LeftButton:  return 1;
    case Qt::MidButton:   return 2;
    case Qt::RightButton: return 3;
    default:              return 0;
    }
}

//----------------------------------------------------------
osgGA::GUIEventAdapter::ScrollingMotion QEventAdapter::Private::remapWheel(QWheelEvent* event) const
//----------------------------------------------------------
{
    if (event->orientation() == Qt::Vertical)
        return (event->delta()>0
                ? osgGA::GUIEventAdapter::SCROLL_UP
                : osgGA::GUIEventAdapter::SCROLL_DOWN);
    else
        return (event->delta()>0
                ? osgGA::GUIEventAdapter::SCROLL_LEFT
                : osgGA::GUIEventAdapter::SCROLL_RIGHT);
}

//----------------------------------------------------------
osgGA::GUIEventAdapter::TouchPhase QEventAdapter::Private::remapGesture(Qt::GestureState state) const
//----------------------------------------------------------
{
    osgGA::GUIEventAdapter::TouchPhase touchPhase = osgGA::GUIEventAdapter::TOUCH_UNKNOWN;
    switch (state) {
    case Qt::GestureStarted:
        touchPhase = osgGA::GUIEventAdapter::TOUCH_BEGAN;
        break;
    case Qt::GestureUpdated:
        touchPhase = osgGA::GUIEventAdapter::TOUCH_MOVED;
        break;
    case Qt::GestureFinished:
    case Qt::GestureCanceled:
        touchPhase = osgGA::GUIEventAdapter::TOUCH_ENDED;
        break;
    case Qt::NoGesture:
        touchPhase = osgGA::GUIEventAdapter::TOUCH_UNKNOWN;
        break;
    };

    return touchPhase;
}

//----------------------------------------------------------
unsigned int QEventAdapter::Private::remapModifiers(Qt::KeyboardModifiers modifiers) const
//----------------------------------------------------------
{
    unsigned int mask = 0;
    if (modifiers & Qt::ShiftModifier)   mask |= osgGA::GUIEventAdapter::MODKEY_SHIFT;
    if (modifiers & Qt::ControlModifier) mask |= osgGA::GUIEventAdapter::MODKEY_CTRL;
    if (modifiers & Qt::AltModifier)     mask |= osgGA::GUIEventAdapter::MODKEY_ALT;
    return mask;
}

//----------------------------------------------------------
std::pair<int, int> QEventAdapter::Private::remapXY(QMouseEvent* event) const
//----------------------------------------------------------
{
    const int dpr = widget_->devicePixelRatio();
    if (fixedSize_)
        return { event->x()*dpr - (widget_->width()*dpr - width_) / 2, height_ - (event->y()*dpr - (widget_->height()*dpr - height_) / 2) };
    else
        return { event->x()*dpr, height_ - event->y()*dpr - 1 };
}

//----------------------------------------------------------
QEventAdapter::QEventAdapter(ViewerWidgetQt* widget, osgGA::EventQueue* eventq)
//----------------------------------------------------------
    : p_(new Private(widget, eventq))
{
    widget->installEventFilter(p_.get());
}

//----------------------------------------------------------
QEventAdapter::~QEventAdapter()
//----------------------------------------------------------
{
}

//----------------------------------------------------------
void QEventAdapter::setSize(int width, int height, bool fixed)
//----------------------------------------------------------
{
    p_->setSize(width, height, fixed);
}

//----------------------------------------------------------
QSize QEventAdapter::size() const
//----------------------------------------------------------
{
    return p_->size();
}

//----------------------------------------------------------
bool QEventAdapter::fixedSize() const
//----------------------------------------------------------
{
    return p_->fixedSize();
}

//----------------------------------------------------------
void QEventAdapter::Private::setSize(int width, int height, bool fixed)
//----------------------------------------------------------
{
    if (width_ != width || height_ != height) {
        const int dpr = widget_->devicePixelRatio();
        QResizeEvent event({width/dpr, height/dpr}, {width_/dpr, height_/dpr});
        resizeEvent(&event);
    }
    fixedSize_ = fixed;
}

//----------------------------------------------------------
QSize QEventAdapter::Private::size() const
//----------------------------------------------------------
{
    return { width_, height_ };
}

//----------------------------------------------------------
bool QEventAdapter::Private::fixedSize() const
//----------------------------------------------------------
{
    return { fixedSize_ };
}

//----------------------------------------------------------
bool QEventAdapter::Private::eventFilter(QObject*, QEvent* event)
//----------------------------------------------------------
{
    if (!widget_->isEnabled())
        return false;

    switch (event->type()) {
    case QEvent::MouseMove:
        mouseMoveEvent((QMouseEvent*)event);
        break;

    case QEvent::MouseButtonPress:
        mousePressEvent((QMouseEvent*)event);
        break;

    case QEvent::MouseButtonRelease:
        mouseReleaseEvent((QMouseEvent*)event);
        break;

    case QEvent::MouseButtonDblClick:
        mouseDoubleClickEvent((QMouseEvent*)event);
        break;

    case QEvent::Wheel:
        wheelEvent((QWheelEvent*)event);
        break;

    case QEvent::KeyPress:
        keyPressEvent((QKeyEvent *)event);
        break;

    case QEvent::KeyRelease:
        keyReleaseEvent((QKeyEvent*)event);
        break;

/*
    case QEvent::FocusIn:
        focusInEvent((QFocusEvent*)event);
        break;

    case QEvent::FocusOut:
        focusOutEvent((QFocusEvent*)event);
        break;

    case QEvent::Enter:
        enterEvent(event);
        break;

    case QEvent::Leave:
        leaveEvent(event);
        break;
*/

    case QEvent::Move:
        moveEvent((QMoveEvent*)event);
        break;

    case QEvent::Resize:
        if (!fixedSize_)
            resizeEvent((QResizeEvent*)event);
        break;

/*
    case QEvent::Close:
        closeEvent((QCloseEvent *)event);
        break;
*/

    case QEvent::Show:
        showEvent((QShowEvent*) event);
        break;

/*
    case QEvent::Hide:
        hideEvent((QHideEvent*) event);
        break;
*/

    default:
        break;
    }

    return false;
}

//----------------------------------------------------------
void QEventAdapter::Private::resizeEvent(QResizeEvent* event)
//----------------------------------------------------------
{
    const int dpr = widget_->devicePixelRatio();
    width_ = event->size().width()*dpr;
    height_ = event->size().height()*dpr;

    eventq_->windowResize(0, 0, width_, height_);
}

//----------------------------------------------------------
void QEventAdapter::Private::showEvent(QShowEvent* event)
//----------------------------------------------------------
{
    const int dpr = widget_->devicePixelRatio();

    if (!fixedSize_) {
        width_ = widget_->width()*dpr;
        height_ = widget_->height()*dpr;
    }

    // Synthetic resize
    QResizeEvent qre({width_/dpr, height_/dpr}, {width_/dpr, height_/dpr});
    resizeEvent(&qre);
}

//----------------------------------------------------------
void QEventAdapter::Private::moveEvent(QMoveEvent* event)
//----------------------------------------------------------
{
    /*
    // Window has been moved to a screen whith different DPR setting.
    QResizeEvent qre(widget_->size(), widget_->size());
    resizeEvent(&qre);
    */
}

//----------------------------------------------------------
void QEventAdapter::Private::setKeyboardModifierState(QInputEvent* event)
//----------------------------------------------------------
{
    eventq_->getCurrentEventState()->setModKeyMask(remapModifiers(event->modifiers()));
}

//----------------------------------------------------------
void QEventAdapter::Private::keyPressEvent(QKeyEvent* event)
//----------------------------------------------------------
{
    setKeyboardModifierState(event);
    eventq_->keyPress(remapKey(event));
}

//----------------------------------------------------------
void QEventAdapter::Private::keyReleaseEvent(QKeyEvent* event)
//----------------------------------------------------------
{
    setKeyboardModifierState(event);
    eventq_->keyRelease(remapKey(event));
}


//----------------------------------------------------------
void QEventAdapter::Private::mousePressEvent(QMouseEvent* event)
//----------------------------------------------------------
{
    setKeyboardModifierState(event);
    const auto xy = remapXY(event);
    eventq_->mouseButtonPress(xy.first, xy.second, remapBtn(event));
}

//----------------------------------------------------------
void QEventAdapter::Private::mouseReleaseEvent(QMouseEvent* event)
//----------------------------------------------------------
{
    setKeyboardModifierState(event);
    const auto xy = remapXY(event);
    eventq_->mouseButtonRelease(xy.first, xy.second, remapBtn(event));
}

//----------------------------------------------------------
void QEventAdapter::Private::mouseDoubleClickEvent(QMouseEvent* event)
//----------------------------------------------------------
{
    setKeyboardModifierState(event);
    const auto xy = remapXY(event);
    eventq_->mouseDoubleButtonPress(xy.first, xy.second, remapBtn(event));
}

//----------------------------------------------------------
void QEventAdapter::Private::mouseMoveEvent(QMouseEvent* event)
//----------------------------------------------------------
{
    setKeyboardModifierState(event);
    const auto xy = remapXY(event);
    eventq_->mouseMotion(xy.first, xy.second);
}

//----------------------------------------------------------
void QEventAdapter::Private::wheelEvent(QWheelEvent* event)
//----------------------------------------------------------
{
    setKeyboardModifierState(event);
    eventq_->mouseScroll(remapWheel(event));
}

//----------------------------------------------------------
void QEventAdapter::Private::gestureEvent(QGestureEvent* event)
//----------------------------------------------------------
{
    QPinchGesture* pinch = static_cast<QPinchGesture*>(event->gesture(Qt::PinchGesture));
    if (!pinch)
        return;

    const QPointF qcenterf = pinch->centerPoint();
    const float angle = pinch->totalRotationAngle();
    const float scale = pinch->totalScaleFactor();

    const QPoint pinchCenterQt = widget_->mapFromGlobal(qcenterf.toPoint());
    const osg::Vec2 pinchCenter( pinchCenterQt.x(), pinchCenterQt.y() );

    //We don't have absolute positions of the two touches, only a scale and rotation
    //Hence we create pseudo-coordinates which are reasonable, and centered around the
    //real position
    const float radius = (widget_->width()+widget_->height())/4;
    const osg::Vec2 vector( scale*cos(angle)*radius, scale*sin(angle)*radius);
    const int dpr = widget_->devicePixelRatio();
    const osg::Vec2 p0 = (pinchCenter+vector) * dpr;
    const osg::Vec2 p1 = (pinchCenter-vector) * dpr;

    osg::ref_ptr<osgGA::GUIEventAdapter> osgEvent;
    const osgGA::GUIEventAdapter::TouchPhase touchPhase = remapGesture(pinch->state());
    switch (touchPhase) {
    case osgGA::GUIEventAdapter::TOUCH_BEGAN:
        osgEvent = eventq_->touchBegan(0, touchPhase, p0[0], p0[1]);
        break;
    case osgGA::GUIEventAdapter::TOUCH_MOVED:
    case osgGA::GUIEventAdapter::TOUCH_STATIONERY:
        osgEvent = eventq_->touchMoved(0, touchPhase, p0[0], p0[1]);
        break;
    case osgGA::GUIEventAdapter::TOUCH_ENDED:
    case osgGA::GUIEventAdapter::TOUCH_UNKNOWN:
        osgEvent = eventq_->touchEnded(0, touchPhase, p0[0], p0[1], 1);
        break;
    }
    osgEvent->addTouchPoint( 1, touchPhase, p1[0], p1[1] );
}
