/* -*-c++-*- OpenSceneGraph - Copyright (C) 2009 Wang Rui, 2016 Kalkulo AS
 *
 * This library is open source and may be redistributed and/or modified under
 * the terms of the OpenSceneGraph Public License (OSGPL) version 0.0 or
 * (at your option) any later version.  The full license is in LICENSE file
 * included with this distribution, and on the openscenegraph.org website.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * OpenSceneGraph Public License for more details.
*/
#include <KOsgQt/PixelBufferQt.h>

#include <QMutex>
#include <QMutexLocker>
#include <QOpenGLContext>
#include <QOpenGLFramebufferObjectFormat>
#include <QOpenGLFunctions>
#include <QScreen>
#include <QOffscreenSurface>
#include <QThread>
#include <QApplication>

#include <QImage>
#include <QLabel>
#include <iostream>

using namespace KOsgQt;

namespace {

    /** A wrapper for osg::GraphicsThread which unpins the context before it exits. */
    class GraphicsThreadQt : public osg::GraphicsThread
    {
        PixelBufferQt* gc_;
    public:
        GraphicsThreadQt(PixelBufferQt* gc) : gc_(gc) {}
        void run() override
        {
            osg::GraphicsThread::run();
            gc_->getContext()->moveToThread(nullptr);
        }
    };

    QOpenGLFramebufferObjectFormat fbo_format(int samples)
    {
        QOpenGLFramebufferObjectFormat format;
        format.setAttachment(QOpenGLFramebufferObject::CombinedDepthStencil);
        format.setSamples(samples);
        return format;
    }
}

//==========================================================
class PixelBufferQt::FBOPool : public osg::Referenced
//==========================================================
{
public:
    FBOPool(QOpenGLFramebufferObjectFormat format);
    QOpenGLFramebufferObject* takeFromPool();
    void returnToPool(QOpenGLFramebufferObject*);
    void resized(const QSize&);
    const QSize& size() const { return size_; }
private:
    QMutex mutex_;
    std::vector<std::unique_ptr<QOpenGLFramebufferObject>> pool_;
    QOpenGLFramebufferObjectFormat format_;
    QSize size_;
};


//----------------------------------------------------------
PixelBufferQt::FBOPool::FBOPool(QOpenGLFramebufferObjectFormat format)
//----------------------------------------------------------
    : format_(std::move(format))
{
}

//----------------------------------------------------------
QOpenGLFramebufferObject* PixelBufferQt::FBOPool::takeFromPool()
//----------------------------------------------------------
{
    QMutexLocker lock(&mutex_);
    if (pool_.empty()) {
        return new QOpenGLFramebufferObject(size_, format_);
    } else {
        QOpenGLFramebufferObject* result(pool_.back().release());
        pool_.pop_back();
        return result;
    }
}

//----------------------------------------------------------
void PixelBufferQt::FBOPool::returnToPool(QOpenGLFramebufferObject* fbo)
//----------------------------------------------------------
{
    QMutexLocker lock(&mutex_);
    if (fbo->size() == size_) {
        pool_.emplace_back(fbo);
    } else {
        delete fbo;
    }
}

//----------------------------------------------------------
void PixelBufferQt::FBOPool::resized(const QSize& size)
//----------------------------------------------------------
{
    QMutexLocker lock(&mutex_);
    pool_.clear();
    size_ = size;
}

//==========================================================
class PixelBufferQt::FBOFromPool::Shared
//==========================================================
{
public:
    Shared(FBOPool* owner, QOpenGLFramebufferObject* fbo) : pool_(owner), fbo_(fbo) {}

    ~Shared()
    {
        osg::ref_ptr<FBOPool> owner(pool_);
        if (owner)
            owner->returnToPool(fbo_);
        else
            delete fbo_;
    }

    osg::observer_ptr<FBOPool> pool_;
    QOpenGLFramebufferObject* fbo_;
};

//----------------------------------------------------------
PixelBufferQt::FBOFromPool::FBOFromPool(FBOPool* owner, QOpenGLFramebufferObject* fbo)
//----------------------------------------------------------
    : s_(new Shared(owner, fbo))
{
}

//----------------------------------------------------------
PixelBufferQt::FBOFromPool::FBOFromPool()
//----------------------------------------------------------
    : s_(nullptr)
{
}

//----------------------------------------------------------
bool PixelBufferQt::FBOFromPool::valid() const
//----------------------------------------------------------
{
    return (bool)s_;
}

//----------------------------------------------------------
GLuint PixelBufferQt::FBOFromPool::texture() const
//----------------------------------------------------------
{
    return s_->fbo_->texture();
}

//----------------------------------------------------------
int PixelBufferQt::FBOFromPool::width() const
//----------------------------------------------------------
{
    return s_->fbo_->width();
}

//----------------------------------------------------------
int PixelBufferQt::FBOFromPool::height() const
//----------------------------------------------------------
{
    return s_->fbo_->height();
}

//==========================================================
class PixelBufferQt::Private
//==========================================================
{
public:
    QOffscreenSurface surface_;
    QOpenGLContext* shareContext_ = nullptr;
    std::unique_ptr<QOpenGLContext> context_;

    osg::ref_ptr<FBOPool> pool_;
    std::unique_ptr<QOpenGLFramebufferObject> current_fbo_;
};


//----------------------------------------------------------
PixelBufferQt::PixelBufferQt(osg::GraphicsContext::Traits* traits)
//----------------------------------------------------------
    : p_(new Private())
{
    if (!traits) {
        osg::DisplaySettings* ds = osg::DisplaySettings::instance().get();
        traits = new osg::GraphicsContext::Traits(ds);
        traits->x = 0;
        traits->y = 0;
        traits->width = 1;
        traits->height = 1;
        traits->red = 8;
        traits->green = 8;
        traits->blue = 8;
        traits->alpha = 8;
        traits->samples = 4;
        traits->windowDecoration = false;
        traits->pbuffer = true;
        traits->doubleBuffer = false;
        traits->sharedContext = nullptr;
    }
    _traits = traits;

    // The pool is always single-sampled, and backed by a texture (which can be
    // shared with another context). If multisampling, we render to a separate
    // FBO and blit into the shareable FBO.
    p_->pool_ = new FBOPool(fbo_format(0));

    // initialize State
    setState(new osg::State);
    getState()->setGraphicsContext(this);

    // initialize contextID
    if (_traits->sharedContext.valid()) {
        getState()->setContextID(_traits->sharedContext->getState()->getContextID());
        incrementContextIDUsageCount(getState()->getContextID());
    } else {
        getState()->setContextID(osg::GraphicsContext::createNewContextID());
    }

    static const int metatype = qRegisterMetaType<FBOFromPool>("FBOFromPool");
    Q_UNUSED(metatype);
}

//----------------------------------------------------------
PixelBufferQt::~PixelBufferQt()
//----------------------------------------------------------
{
}

//----------------------------------------------------------
void PixelBufferQt::setShareContext(QOpenGLContext* shareContext)
//----------------------------------------------------------
{
    p_->shareContext_ = shareContext;
}

//----------------------------------------------------------
QOpenGLContext* PixelBufferQt::getContext()
//----------------------------------------------------------
{
    return p_->context_.get();
}

//----------------------------------------------------------
bool PixelBufferQt::valid() const
//----------------------------------------------------------
{
    // Must return true before we are initialized, otherwise
    // osgViewer::Viewer::realize will try to create a new context (which becomes a full-screen window)
    return true;
    //return ((!p_->context_ || p_->context_->isValid()) && p_->surface_.isValid());
}

//----------------------------------------------------------
void PixelBufferQt::createGraphicsThread()
//----------------------------------------------------------
{
    // If graphics thread is already set, it will not be reset by osgViewer::Viewer::realize
    setGraphicsThread(new GraphicsThreadQt(this));
}

//----------------------------------------------------------
bool PixelBufferQt::realizeImplementation()
//----------------------------------------------------------
{
    if (QThread::currentThread() != QApplication::instance()->thread()) {
        throw std::runtime_error("realize() must be called from the GUI thread");
    }

    // initialize GL context for the widget
    p_->context_.reset(new QOpenGLContext());

    {
        QSurfaceFormat format;

        format.setAlphaBufferSize(_traits->alpha);
        format.setRedBufferSize(_traits->red);
        format.setGreenBufferSize(_traits->green);
        format.setBlueBufferSize(_traits->blue);
        format.setDepthBufferSize(_traits->depth);
        format.setStencilBufferSize(_traits->stencil);
        //format.setSampleBuffers(_traits->sampleBuffers);
        format.setSamples(_traits->samples);
        format.setSwapBehavior(QSurfaceFormat::SingleBuffer);
        //format.setSwapBehavior(_traits->doubleBuffer ? QSurfaceFormat::DoubleBuffer : QSurfaceFormat::SingleBuffer);
        //format.setSwapInterval(_traits->vsync ? 1 : 0);
        format.setStereo(_traits->quadBufferStereo);

        //format.setProfile(QSurfaceFormat::CompatibilityProfile);
        //format.setRenderableType(QSurfaceFormat::OpenGL);
        //format.setOption(QSurfaceFormat::DeprecatedFunctions);

        p_->context_->setFormat(std::move(format));
    }

    try {
        osg::ref_ptr<osg::GraphicsContext> traitsSharedContext(_traits->sharedContext);
        if (traitsSharedContext) {
            auto traitsShareContextQt = dynamic_cast<PixelBufferQt*>(traitsSharedContext.get());
            if (!traitsShareContextQt)
                throw std::runtime_error("Can't share context with non-Qt GraphicsContext");
            if (p_->shareContext_)
                throw std::runtime_error("Multiple share contexts (constructor and traits)");
            p_->context_->setShareContext(traitsShareContextQt->p_->shareContext_);
        } else {
            p_->context_->setShareContext(p_->shareContext_);
        }

        if (!p_->context_->create())
            throw std::runtime_error("context::create() failed in realize()");
        if (!p_->context_->isValid())
            throw std::runtime_error("failed to create valid context");

        // Create surface. [Qt doc]: "In order to create an offscreen surface that
        // is guaranteed to be compatible with a given context and window, make
        // sure to set the format to the context's or the window's actual format,
        // that is, the QSurfaceFormat returned from QOpenGLContext::format() or
        // QWindow::format() after the context or window has been created." We
        // assume the format is compatible between the widget and the renderer,
        // since both are off-screen. (NOTE: Does that mean we should parse traits
        // for the widget, too? )

        // [Qt doc]: "Note: Due to the fact that QOffscreenSurface is backed by
        // a QWindow on some platforms, cross-platform applications must ensure
        // that create() is only called on the main (GUI) thread. The
        // QOffscreenSurface is then safe to be used with makeCurrent() on
        // other threads, but the initialization and destruction must always
        // happen on the main (GUI) thread."

        p_->surface_.setFormat(p_->context_->format());
        p_->surface_.create();
        if (!p_->surface_.isValid())
            throw std::runtime_error("surface::create() failed in realize()");

        /* Unpin the context from this thread, so that it can be pinned to
         * another thread in makeCurrent */
        p_->context_->moveToThread(nullptr);

        return true;
    } catch (std::runtime_error &e) {
        OSG_WARN << "PixelBufferQt::realizeImpl(): " << e.what() << std::endl;
        close();
        return false;
    }
}

//----------------------------------------------------------
bool PixelBufferQt::isRealizedImplementation() const
//----------------------------------------------------------
{
    return (p_->context_ != nullptr);
}

//----------------------------------------------------------
void PixelBufferQt::closeImplementation()
//----------------------------------------------------------
{
    p_->context_.reset(nullptr);
}

//----------------------------------------------------------
void PixelBufferQt::runOperations()
//----------------------------------------------------------
{
    if (!p_->pool_->size().isValid()) {
        OSG_WARN << "PixelBufferQt::runOperations(): not sized yet\n";
        return;
    }

    // Create (or re-create) and bind the current fbo if necessary.
    if (!p_->current_fbo_ || p_->current_fbo_->size() != p_->pool_->size()) {
        if (_traits->samples == 0) {
            p_->current_fbo_.reset(p_->pool_->takeFromPool());
        } else {
            p_->current_fbo_.reset(new QOpenGLFramebufferObject(p_->pool_->size(), fbo_format(_traits->samples)));
        }
        if (!p_->current_fbo_->bind()) throw std::runtime_error("fbo->bind() failed");

        // Set the default FBO for osg to switch back to
        setDefaultFboId(p_->current_fbo_->handle());
    }

    osg::GraphicsContext::runOperations();
}

//----------------------------------------------------------
bool PixelBufferQt::makeCurrentImplementation()
//----------------------------------------------------------
{
    QThread* ctx_thread = p_->context_->thread();
    QThread* cur_thread = QThread::currentThread();
    if (ctx_thread == nullptr) {
        p_->context_->moveToThread(cur_thread);
        if (p_->context_->makeCurrent(&p_->surface_)) {
            emit bringup();
            return true;
        } else {
            p_->context_->moveToThread(nullptr);
            return false;
        }
    } else if (ctx_thread != cur_thread) {
        OSG_WARN << "PixelBufferQt::makeCurrent: skipping makeCurrent, wrong thread\n";
        return false;
    }

    return p_->context_->makeCurrent(&p_->surface_);
}

//----------------------------------------------------------
bool PixelBufferQt::makeContextCurrentImplementation(GraphicsContext*)
//----------------------------------------------------------
{
    // What is this supposed to do?
    OSG_WARN << "PixelBufferQt::makeContextCurrent(GraphicsContext*) not implemented\n";
    return false;
}

//----------------------------------------------------------
bool PixelBufferQt::releaseContextImplementation()
//----------------------------------------------------------
{
    p_->context_->doneCurrent();
    return true;
}

//----------------------------------------------------------
void PixelBufferQt::swapBuffersImplementation()
//----------------------------------------------------------
{
    if (!p_->current_fbo_) return;

#if 0
    static QLabel label;
    QImage img(p_->current_fbo_->toImage());
    label.resize(img.size());
    label.setPixmap(QPixmap::fromImage(img));
    label.show();
#endif

    if (p_->current_fbo_->format().samples() > 0) {
        // Multisampling requested, create a texture-backed (single-sampled) fbo
        // Blit to texture and rebind current fbo.
        std::unique_ptr<QOpenGLFramebufferObject> blit_fbo(p_->pool_->takeFromPool());
        QOpenGLFramebufferObject::blitFramebuffer(blit_fbo.get(), p_->current_fbo_.get(), GL_COLOR_BUFFER_BIT);
        if (!p_->current_fbo_->bind()) throw std::runtime_error("fbo->bind() failed after blit");

        // Emit the blitted fbo.

        // In multithreaded mode, the glFinish is required here - otherwise,
        // the frame may render the old content of the FBO. In principle it
        // could be more efficient to do the blit earlier in the pipeline, but
        // simply placing it in runOperations didn't help - see
        // swapReadyBarrier in osgViewer::ViewerBase::startThreading, it uses a
        // NO_OPERATION instead of GL_FINISH, I don't understand how/where
        // other operations are flushed. It's not a big deal.
        QOpenGLContext::currentContext()->functions()->glFinish();

        emit newFrame(FBOFromPool(p_->pool_, blit_fbo.release()));
    } else {
        // No blitting was required, just emit the current fbo
        QOpenGLContext::currentContext()->functions()->glFinish();
        emit newFrame(FBOFromPool(p_->pool_, p_->current_fbo_.release()));
    }
}

//----------------------------------------------------------
void PixelBufferQt::resizedImplementation(int x, int y, int width, int height)
//----------------------------------------------------------
{
    if (width > 0 && height > 0) { // We sometimes receive events with height==0 which screw up stuff in osg::GraphicsContext::resizedImplementation
        // Invalidate the FBO pool - this will cause runOperations to bind a new fbo
        const QSize size(width, height);
        if (size != p_->pool_->size()) {
            p_->pool_->resized(size);
        }
        osg::GraphicsContext::resizedImplementation(x, y, width, height);
    }
}

//----------------------------------------------------------
void PixelBufferQt::bindPBufferToTextureImplementation(GLenum buffer)
//----------------------------------------------------------
{
    // What is this supposed to do?
    OSG_WARN << "PixelBufferQt::bindPBufferToTextureImplementation(GLenum buffer) not implemented\n";
}
